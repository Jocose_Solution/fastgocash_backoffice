﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HotelSearch.ascx.vb"
    Inherits="UserControl_HotelSearch" %>
<link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet"
    type="text/css" />
<link href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet"
    type="text/css" />

<script src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>" type="text/javascript"></script>

<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>" type="text/javascript"></script>

<script src="<%=ResolveUrl("~/Hotel/JS/HtlSearchQuery.js") %>" type="text/javascript"></script>

<script type="text/javascript">
    var UrlBase = '<%=ResolveUrl("~/") %>';
    $(function () {
        var HtlDatePickerOption = { numberOfMonths: 2, dateFormat: "dd/mm/dd", maxDate: "+1y", minDate: "+2d", showOtherMonths: true, selectOtherMonths: false };
        $("#htlcheckin").datepicker(HtlDatePickerOption).datepicker("setDate", new Date());
        $("#htlcheckout").datepicker(HtlDatePickerOption).datepicker("setDate", new Date().getDate + 3);
    });
</script>

<div class="w100 serchbox mtop20">
    <div class="row">
        <div class="tital large-12 medium-12 small-12">Book Domestic And International Hotels</div>
        <div class="titalr large-12 medium-12 small-12">Book Hotels</div>
        <div class="clear"></div>
        <div class="large-12 medium-12 small-12">
            <div class="large-5 medium-5 small-12 columns">

                <div class="large-10 medium-10 small-12 columns">
                    Select City:<br />
                    <input type="text" id="htlCity" name="htlCity" value="" placeholder="Enter a city or area name"
                        data-trauncate="false" title="Where do you want to go" />
                    <input type="hidden" id="htlcitylist" name="htlcitylist" value="" />
                    <input type="hidden" id="contrycode" name="contrycode" value="IN" />
                </div>
                <div class="clear"></div>

                <div class="large-4 medium-4 small-6 columns">
                    Check In Date:<br />
                    <input type="text" id="htlcheckin" name="htlcheckin" value="" class="txtCalander"
                        readonly="readonly" />
                    <input type="hidden" name="hidhtlcheckin" id="hidhtlcheckin" value="" />
                </div>


                <div class="large-4 medium-4 small-6 large-push-2 medium-push-2 columns">
                    Check Out Date:<br />
                    <input type="text" id="htlcheckout" name="htlcheckout" value="" class="txtCalander"
                        readonly="readonly" />
                    <input type="hidden" name="hidhtlcheckout" id="hidhtlcheckout" value="" />
                    <div style="float: right;" id="nights">1 Night</div>
                </div>
                <div class="clear"></div>

                <div class="large-12 medium-12 small-12">
                    <a href="#" id="buttonAddOpt"><span class="aditional">Additional Option</span></a>
                    <div class="large-12 medium-12 small-12" id="effect" style="display: none;">
                        <div>


                            <div class="large-10 medium-10 small-12 columns end">
                                Hotel Name:<br />
                                <input type="text" id="htlname" name="htlname" value="" placeholder="Enter a hotel name"
                                    data-trauncate="false" title="Where do you want to stay" />
                                <input type="hidden" id="Hotelcode" name="Hotelcode" value="" />
                            </div>
                            <div class="clear"></div>

                            <div class="large-10 medium-10 small-12 columns end">
                                Star Rating:<br />
                                <select id="htlstar" name="htlstar" title="Hotel Class">
                                    <option value="0">Select Star Rating</option>
                                    <option value="1">1 Star</option>
                                    <option value="2">2 Stars</option>
                                    <option value="3">3 Stars</option>
                                    <option value="4">4 Stars</option>
                                    <option value="5">5 Stars</option>
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



            <div class="large-5 medium-5 small-12 columns">
                <div class="hoteltitleul">
                    
                        <span style="color: #fff" class="hoteltitleli3 bld f16">Number of Rooms & Guests</span>
                    
                </div>
                <div id="hot-search-params">
                </div>
                <input type="hidden" name="rooms" id="rooms" />
                <input type="hidden" name="chds" id="chds" />
                <div class="clear"></div>

                <div class="large-4 medium-4 small-12 rgt">
                    <input type="hidden" name="ReqType" id="ReqType" value="S" />
                    <input type="button" id="btnHotel" value="Find Hotel" class="button" />
                    </>
                </div>

            </div>

        </div>

    </div>
</div>

<script src="Hotel/JS/hotelpasg.js" type="text/javascript"></script>
