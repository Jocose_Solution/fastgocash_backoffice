﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class SprReports_Money_Transfer_payout_direct_markup_charges : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect("~/Login.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                BindGroupType();
                BindGridview();
            }
        }
    }

    protected void BindGridview()
    {
        SqlDataAdapter sda = new SqlDataAdapter("select * from  T_PayoutMarkupCharges", con);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        dmt_grid.DataSource = dt;
        dmt_grid.DataBind();
    }

    private void BindGroupType()
    {
        try
        {
            ddlGroupType.Items.Clear();

            ddlGroupType.DataSource = STDom.GetAllGroupType().Tables[0];
            ddlGroupType.DataTextField = "GroupType";
            ddlGroupType.DataValueField = "GroupType";
            ddlGroupType.DataBind();
            ddlGroupType.Items.Insert(0, new ListItem("Select Type", ""));
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            SqlCommand cmd = new SqlCommand("sp_PayoutMarkupCharges", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Group_Type", ddlGroupType.SelectedValue);
            cmd.Parameters.AddWithValue("Charges_Type", ddlChargeType.SelectedValue);
            cmd.Parameters.AddWithValue("Amount", txtAmount.Text);
            cmd.Parameters.AddWithValue("Charges", txtCharges.Text);
            cmd.Parameters.AddWithValue("type", "insert");
            cmd.Parameters.AddWithValue("UpdatedBy", Session["UID"].ToString());

            con.Open();
            int k = cmd.ExecuteNonQuery();

            if (k > 0)
            {
                ddlGroupType.SelectedIndex = 0;
                ddlChargeType.SelectedIndex = 0;
                txtAmount.Text = "";
                txtCharges.Text = "";

                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record submited successfully.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record already Exists.');", true);
            }
            con.Close();
            BindGridview();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void dmt_grid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string commandName = e.CommandName;

        if (commandName == "delete")
        {
            int id = Convert.ToInt32(e.CommandArgument);
            using (SqlCommand cmd = new SqlCommand("sp_PayoutMarkupCharges"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Id", id);
                cmd.Parameters.AddWithValue("@type", "delete");
                cmd.Connection = con;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();

                this.BindGridview();

                if (i > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record deleted successfully.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record not deleted.');", true);
                }
            }
        }
    }

    protected void dmt_grid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
}