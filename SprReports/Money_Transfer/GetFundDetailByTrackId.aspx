﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GetFundDetailByTrackId.aspx.cs" Inherits="SprReports_Money_Transfer_GetFundDetailByTrackId" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Update Credit and Debit</title>
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <script src="../../JS/JScript.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">       
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tbl_Upload" runat="server">
            <tr>
                <td>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="padding-right: 17px">
                                    <fieldset style="border: thin solid #004b91; padding-left: 10px">
                                        <legend style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #004b91;">INFROMATION</legend>
                                        
                                        <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                            <tr>                                                
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Track Id :</td>
                                                <td id="td_TrackId" runat="server"></td>
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">ipay_id :</td>
                                                <td id="td_ipay_id" runat="server">
                                                    <%=td_ipay_id_str %>
                                                </td>
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">opr_id :</td>
                                                <td id="td_opr_id" runat="server"><%=td_opr_id_str %></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8">&nbsp;</td>
                                            </tr>
                                            <tr>   
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Txn Date :</td>
                                                <td id="td_TxnDate" runat="server"></td>
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Agent Id :</td>
                                                <td id="td_AgentId" runat="server"></td>
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Agency Name :</td>
                                                <td id="td_Agency_Name" runat="server"></td>                                                
                                            </tr>
                                            <tr>
                                                <td colspan="8">&nbsp;</td>
                                            </tr>
                                            <tr>                                                
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Remitter Mobile :</td>
                                                <td id="td_RemitterMobile" runat="server"></td>
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Benificiery Id :</td>
                                                <td id="td_BenificieryId" runat="server"></td>
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Benificiery Name :</td>
                                                <td id="td_BenificieryName" runat="server"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8">&nbsp;</td>
                                            </tr>
                                            <tr>                                                
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Request Amt :</td>
                                                <td id="td_RequestAmount" runat="server"><%=reqAmt %></td>
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">API Charged Amt :</td>
                                                <td id="td_Charged_Amount" runat="server"><%=chAmt %></td>
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Ledger Debit Amt :</td>
                                                <td id="td_ledDebit" runat="server"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8">&nbsp;</td>
                                            </tr>
                                            <tr>         
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Bank Alias :</td>
                                                <td id="td_bank_alias" runat="server"></td>
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Txn Mode :</td>
                                                <td id="td_TxnMode" runat="server"></td>
                                                <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;">Status :</td>
                                                <td id="td_Status" runat="server"></td>                                              
                                            </tr>
                                        </table>
                                        
                                        
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
      
    </form>
</body>
</html>
