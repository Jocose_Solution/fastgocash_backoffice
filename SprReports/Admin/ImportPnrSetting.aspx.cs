﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class SprReports_Admin_ImportPnrSetting : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    public string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            try
            {
                if (!IsPostBack)
                {
                    ddl_ptype.AppendDataBoundItems = true;
                    ddl_ptype.Items.Clear();
                    ddl_ptype.DataSource = STDom.GetAllGroupType().Tables[0];
                    ddl_ptype.DataTextField = "GroupType";
                    ddl_ptype.DataValueField = "GroupType";
                    ddl_ptype.DataBind();
                    ddl_ptype.Items.Insert(0, new ListItem("-- Select Type --", "ALL"));
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    public void BindGrid()
    {
        try
        {
            grid_PnrSetting.DataSource = GetRecord();
            grid_PnrSetting.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetRecord()
    {
        DataTable dt = new DataTable();
        try
        {
            string TripType = Convert.ToString(DdlTripType.SelectedValue);
            adap = new SqlDataAdapter("SpFlightImportPnrSetting", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@GroupType", ddl_ptype.SelectedValue);
            adap.SelectCommand.Parameters.AddWithValue("@TripType", TripType);
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "select");
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }
    private int Insert(string GroupType, string AgentId, string AirCode, string AirlineName, string TripType, string TripTypeName, string IsActive, double Charges)
    {
        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        string ActionType = "insert";
        try
        {
            SqlCommand cmd = new SqlCommand("SpFlightImportPnrSetting", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@GroupType", GroupType);
            cmd.Parameters.AddWithValue("@AgentId", AgentId);
            cmd.Parameters.AddWithValue("@AirCode", AirCode);
            cmd.Parameters.AddWithValue("@AirlineName", AirlineName);
            cmd.Parameters.AddWithValue("@TripType", TripType);
            cmd.Parameters.AddWithValue("@TripTypeName", TripTypeName);
            cmd.Parameters.AddWithValue("@Charges", Charges);
            cmd.Parameters.AddWithValue("@IsActive", Convert.ToBoolean(IsActive));
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return flag;
    }
    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Convert.ToString(TxtCharges.Text)))
            {
                #region Insert
                string GroupType = ddl_ptype.SelectedValue;
                string AgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
                if (!string.IsNullOrEmpty(AgentId))
                {
                    GroupType = "";
                }
                string Airline = Convert.ToString(Request["hidtxtAirline"]);
                string FlightName = Convert.ToString(Request["txtAirline"]);
                string AirCode = "";
                string AirlineName = "";

                if (!string.IsNullOrEmpty(FlightName))
                {
                    if (!string.IsNullOrEmpty(Airline))
                    {
                        AirlineName = Airline.Split(',')[0];
                        if (Airline.Split(',').Length > 1)
                        {
                            AirCode = Airline.Split(',')[1];
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select again airline!!');", true);
                            return;
                        }
                    }
                }
                else
                {
                    AirlineName = "ALL";
                    AirCode = "ALL";
                }
                msgout = "";
                string TripType = Convert.ToString(DdlTripType.SelectedValue);
                string TripTypeName = Convert.ToString(DdlTripType.SelectedItem.Text);
                string IsActive = Convert.ToString(DdlStatus.SelectedValue);
                double Charges = 0;
                Charges = Convert.ToDouble(TxtCharges.Text);
                int flag = Insert(GroupType, AgentId, AirCode, AirlineName, TripType, TripTypeName, IsActive, Charges);
                if (flag > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');", true);
                    BindGrid();
                }
                else
                {
                    if (msgout == "EXISTS")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('already exists.');", true);
                        BindGrid();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');window.location='ImportPnrSetting.aspx'; ", true);
                    }
                }
                #endregion
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter Charges!!');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='ImportPnrSetting.aspx'; ", true);
            return;
        }
    }
    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int flag = 0;
            try
            {
                Label lblSNo = (Label)(grid_PnrSetting.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());
                SqlCommand cmd = new SqlCommand("SpFlightImportPnrSetting", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", Id);
                cmd.Parameters.AddWithValue("@ActionType", "delete");
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (SqlException ex)
            {
                con.Close();
                clsErrorLog.LogInfo(ex);
            }
            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void grid_PnrSetting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grid_PnrSetting.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }
   
    protected void grid_PnrSetting_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            grid_PnrSetting.EditIndex = e.NewEditIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void grid_PnrSetting_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            grid_PnrSetting.EditIndex = -1;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
}