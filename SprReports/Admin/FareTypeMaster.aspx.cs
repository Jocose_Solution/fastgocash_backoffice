﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_FareTypeMaster : System.Web.UI.Page
{
    DataTable dt = new DataTable();
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    #region New Concept Code
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        DivMsg.InnerHtml = "";
        DivMsgExclued.InnerHtml = "";
        if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            try
            {
                if (!IsPostBack)
                {
                    FareDDl.AppendDataBoundItems = true;
                    FareDDl.Items.Clear();
                    //Dim item As New ListItem("All Type", "0")
                    //ddl_ptype.Items.Insert(0, item)
                    FareDDl.DataSource = GetRecord(0, "", "ALL");//STDom.GetAllGroupType().Tables[0];
                    FareDDl.DataTextField = "FareType";
                    FareDDl.DataValueField = "Id";
                    FareDDl.DataBind();
                    FareDDl.Items.Insert(0, new ListItem("-- Select Type --", "0"));
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }


    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        #region Update
        try
        {
            //int Id = 0;
            string FareTypeId = FareDDl.SelectedValue;
            string FareType = FareDDl.SelectedItem.Text;
            string DisplayName = txtDisplayResultName.Text;            
            string AddMsg = "";
            msgout = "";
            DivMsg.InnerHtml = "";
            string Remark = txtRemark.Text;
           string ActionType = "update";
            if (!string.IsNullOrEmpty(DisplayName) && FareTypeId !="0")
            {
                #region Insert PG Charges Agent UserId Wise
                int flag = 0;
                flag = InsertAndUpdate(Convert.ToInt32(FareTypeId), FareType, DisplayName, Remark,ActionType);
                if (flag > 0)//0 || !string.IsNullOrEmpty(msgout)) //(!string.IsNullOrEmpty(msgout))
                {
                    //DivMsg.InnerHtml = msgout;
                    //hidActionType.Value = "select";
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert(" + msgout + ");", true);

                    string message = "Updated Successfully.";
                    string script = "window.onload = function(){ alert('";
                    script += message;
                    script += "')};";
                    ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                    BindGrid();
                   
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again');", true);
                    hidActionType.Value = "select";
                    BindGrid();
                }
                #endregion Insert PG Charges AgentWise
            }
            else
            {
                DivMsg.InnerHtml = "";
                AddMsg = "Please Select Fare Type and Enter Display Name";              
                DivMsg.InnerHtml = AddMsg;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert(" + AddMsg + ");", true);
            }
        #endregion
    }
    catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='PGCharges.aspx'; ", true);
            return;
        }
    #endregion
    }   
    public void BindGrid()
    {
        try
        {
            grd_P_IntlDiscount.DataSource = GetRecord(0, "", "ALL");
            grd_P_IntlDiscount.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetRecord(int Id, string FareType, string ActionType)
    { 
        DataTable dt = new DataTable();
        try
        {
            //string ActionType = hidActionType.Value;
            if (con.State == ConnectionState.Closed)
                con.Open();
            adap = new SqlDataAdapter("USP_FARETYPEMASTER_UPADTE", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@Id", Id);
            adap.SelectCommand.Parameters.AddWithValue("@FareType", FareType);
            adap.SelectCommand.Parameters.AddWithValue("@Action", ActionType);
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            con.Close();
            adap.Dispose();
        }
        return dt;
    }
    private int InsertAndUpdate(int Id, string FareType, string DisplayName, string Remark, string ActionType)
    {
        int flag = 0;
        string ActionBy = Convert.ToString(Session["UID"]);
        try
        {
            SqlCommand cmd = new SqlCommand("USP_FARETYPEMASTER_UPADTE", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", Id);
            cmd.Parameters.AddWithValue("@FareType", FareType);
            cmd.Parameters.AddWithValue("@DisplayName", DisplayName);
            cmd.Parameters.AddWithValue("@Remark", Remark);
            cmd.Parameters.AddWithValue("@ActionBy", ActionBy);
            cmd.Parameters.AddWithValue("@Action", ActionType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return flag;

    }   
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd_P_IntlDiscount.PageIndex = e.NewPageIndex;
        //ActionTypeGrid = "select";        
        this.BindGrid();
    }
    protected void FareDDl_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(FareDDl.SelectedValue) && FareDDl.SelectedValue != "0")
            {
                DataTable dt = GetRecord(Convert.ToInt32(FareDDl.SelectedValue), "", "GetDetails");
                if (dt != null && dt.Rows.Count > 0)
                {
                    string DisplayName = Convert.ToString(dt.Rows[0]["DisplayName"]);
                    txtDisplayResultName.Text = DisplayName;
                }
                else
                {
                    txtDisplayResultName.Text = "";                
                }
            }
        }
        catch (Exception ex)
        { }

    }
}