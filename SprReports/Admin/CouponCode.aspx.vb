﻿Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data.SqlClient

Partial Class SprReports_Admin_CouponCode
    Inherits System.Web.UI.Page
    Private con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Private adap As SqlDataAdapter
    Private ds As New DataSet()

    Private dt As New DataTable()


    Public Function GroupTypeMGMT(ByVal type As String, ByVal desc As String, ByVal cmdType As String, ByRef msg As String) As DataTable
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim dt As New DataTable()
        Try

            con.Open()

            Dim cmd As New SqlCommand()

            cmd.CommandText = "usp_agentTypeMGMT"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 200).Value = type
            cmd.Parameters.Add("@desc", SqlDbType.VarChar, 500).Value = desc
            cmd.Parameters.Add("@cmdType", SqlDbType.VarChar, 50).Value = cmdType
            cmd.Parameters.Add("@msg", SqlDbType.VarChar, 500)
            cmd.Parameters("@msg").Direction = ParameterDirection.Output

            cmd.Connection = con
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            msg = cmd.Parameters("@msg").Value.ToString().Trim()



            con.Close()


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            con.Close()

        End Try
        Return dt
    End Function
    Public Sub reset()
        ddl_airline.SelectedIndex = 0
        ddl_TripType.SelectedIndex = 0
        'TXTAgentId.Text = ""
        DdlPaxWise.SelectedIndex = 0
        TXTOrg.Text = ""
        TXTAmount.Text = "0"
        TXTDest.Text = ""
        drpMarkupType.SelectedValue = "0"
        txt_Code.Text = "0"
    End Sub

    Protected Sub SAVE_Click(Sender As [Object], e As EventArgs)
        Try
            If (drpMarkupType.SelectedValue = "0") Then
                ShowAlertMessage("Please select markup type. ")
            Else
                '    ShowAlertMessage("Unable to insert .Please try again ")
                'End If

             

                Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "ALL", "ALL", Request("hidtxtAgencyName"))

                'Checking for entry
                Dim cmd1 As New SqlCommand("Sp_Check_Tbl_Coupon", con)
                con.Open()
                cmd1.CommandType = CommandType.StoredProcedure
                cmd1.Parameters.AddWithValue("@COUPONCODE", txt_Code.Text.Trim)
                Dim st As Boolean = cmd1.ExecuteScalar()
                con.Close()
                'End Checking for entry
                If (st = False) Then
                    Dim cmd As New SqlCommand("Sp_Insert_Tbl_Coupon", con)
                    con.Open()
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@Airline", ddl_airline.SelectedValue)
                    cmd.Parameters.AddWithValue("@Trip", ddl_TripType.SelectedValue)
                    cmd.Parameters.AddWithValue("@AgentId", AgentID)
                    cmd.Parameters.AddWithValue("@Amount", Convert.ToDecimal(TXTAmount.Text))
                    cmd.Parameters.AddWithValue("@Org", TXTOrg.Text)
                    cmd.Parameters.AddWithValue("@Dest", TXTDest.Text)
                    cmd.Parameters.AddWithValue("@CouponOn", DD_FARE.SelectedValue)
                    cmd.Parameters.AddWithValue("@CreatedBy", Session("UID"))
                    cmd.Parameters.AddWithValue("@MarkupType", drpMarkupType.SelectedValue)
                    cmd.Parameters.AddWithValue("@CouponOnPaxWise", DdlPaxWise.SelectedValue)
                    cmd.Parameters.AddWithValue("@CouponCode", txt_Code.Text.Trim)
                    cmd.Parameters.AddWithValue("@StartDate", Request("From").ToString())
                    cmd.Parameters.AddWithValue("@EndDate", Request("To").ToString())
                    cmd.Parameters.AddWithValue("@CouponCount", Convert.ToInt32(CouponCount.Text.Trim))
                    cmd.Parameters.AddWithValue("@TravelType", DD_TRAVEL.SelectedValue)

                    Dim i As Integer = cmd.ExecuteNonQuery()
                    con.Close()
                    If (i > 0) Then
                        ShowAlertMessage("Coupon Code submitted successfully")

                    Else
                        ShowAlertMessage("Unable to insert .Please try again ")
                    End If
                    getgv()
                    reset()
                Else
                    ShowAlertMessage("Coupon Code already applied.")
                End If
                drpMarkupType.SelectedValue = "0"
                DD_FARE.SelectedValue = "0"
                'ShowAlertMessage("Unable to insert .Please try again ")
            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub btnreset_Click(sender As Object, e As EventArgs)
        reset()
    End Sub
    Private Sub getgv()
        Try
            Dim da As New SqlDataAdapter("sp_TBL_Coupon", con)
            da.Fill(ds)
            grdemp.DataSource = ds
            grdemp.DataBind()
        Catch ex As Exception
        End Try

    End Sub
    Protected Sub grdemp_RowEditing(sender As Object, e As GridViewEditEventArgs)
        Try
            grdemp.EditIndex = e.NewEditIndex


            getgv()
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub grdemp_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        Try
            Dim lbtnCounter As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtnCounter"), Label), Label)
            Dim txtAmount As TextBox = TryCast(grdemp.Rows(e.RowIndex).FindControl("txtAmount"), TextBox)
            Dim txtcount As TextBox = TryCast(grdemp.Rows(e.RowIndex).FindControl("TXCouponCount"), TextBox)
            Dim GDD_TRAVEL As DropDownList = CType(grdemp.Rows(e.RowIndex).FindControl("GDD_TRAVEL"), DropDownList)

            If (txtAmount.Text <> "" And txtcount.Text <> "" And txtAmount.Text <> 0 And txtcount.Text <> 0) Then

                If Not String.IsNullOrEmpty(Convert.ToString(lbtnCounter.Text)) Then
                    con.Open()
                    Dim cmd As New SqlCommand("Sp_Update_Tbl_Coupon", con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@Counter", Convert.ToInt32(lbtnCounter.Text))
                    cmd.Parameters.AddWithValue("@Amount", Convert.ToDecimal(txtAmount.Text))
                    cmd.Parameters.AddWithValue("@CouponCount", Convert.ToInt32(txtcount.Text))
                    cmd.Parameters.AddWithValue("@UserID", Session("UID"))
                    cmd.Parameters.AddWithValue("@TravelType", GDD_TRAVEL.SelectedValue)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End If
                grdemp.EditIndex = -1
                getgv()
            Else

                Dim message As String = "Can't be null"
                Dim sb As New System.Text.StringBuilder()
                sb.Append("<script type = 'text/javascript'>")
                sb.Append("window.onload=function(){")
                sb.Append("alert('")
                sb.Append(message)
                sb.Append("')};")
                sb.Append("</script>")
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())

            End If


        Catch ex As Exception
            con.Close()
        End Try


    End Sub
    Protected Sub grdemp_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs)


        grdemp.EditIndex = -1
        getgv()
    End Sub
    Protected Sub grdemp_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
        Dim lbtnCounter As Label = TryCast(DirectCast(grdemp.Rows(e.RowIndex).FindControl("lbtnCounter"), Label), Label)
      
        con.Open()
        Dim IPAddress As String = Request.ServerVariables("REMOTE_ADDR")
        Dim UserID As String = Session("UID").ToString()
        Dim cmd As New SqlCommand("Sp_Delete_Tbl_Coupon", con)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@counter", lbtnCounter.Text)
        cmd.ExecuteNonQuery()
        con.Close()
        grdemp.EditIndex = -1
        getgv()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If (Session("UID") = "" Or Session("UID") Is Nothing) Or Session("User_Type") <> "ADMIN" Then
            Response.Redirect("~/Login.aspx")
        End If

        If Not Page.IsPostBack Then
            'grdemp.DataSource = BindGridView();


            Dim msg As String = ""
          

            grdemp.DataBind()

            getgv()


            ddl_airline.AppendDataBoundItems = True
            ddl_airline.Items.Clear()
            '<asp:ListItem Value="ALL">--ALL--</asp:ListItem>
            ddl_airline.Items.Insert(0, New ListItem("-- ALL Airline --", "ALL"))
            ddl_airline.DataSource = GetAirline()
            ddl_airline.DataTextField = "AL_Name"
            ddl_airline.DataValueField = "AL_Code"
            ddl_airline.DataBind()


        End If
    End Sub
    Public Shared Sub ShowAlertMessage(ByVal [error] As String)
        Try


            Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
            If page IsNot Nothing Then
                [error] = [error].Replace("'", "'")
                ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", "alert('" & [error] & "');", True)
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub
    Public Function GetAirline() As DataTable
        Dim dt As DataTable = New DataTable()
        Try
            adap = New SqlDataAdapter("SP_GetAirlinenames", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.Fill(dt)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        Finally
            adap.Dispose()
        End Try

        Return dt
    End Function

  

    Protected Sub grdemp_RowCommand(sender As Object, e As GridViewCommandEventArgs)

    End Sub

    Protected Sub grdemp_RowDataBound1(sender As Object, e As GridViewRowEventArgs)

        '' Dim GGDD_DISTYPE As DropDownList = CType(e.Row.FindControl("GDD_TRAVEL"), DropDownList)
        ''  GGDD_DISTYPE.DataBind()
        '' GGDD_DISTYPE.Items.FindByValue((TryCast(e.Row.FindControl("Elbl_lbTravelType"), Label)).Text).Selected = True

    End Sub
End Class
