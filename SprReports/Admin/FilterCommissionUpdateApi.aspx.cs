﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_FilterCommissionUpdateApi : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])))
        {
            Response.Redirect("~/Login.aspx");
        }
        string Id = Request.QueryString["id"];
        HdnId.Value = Id; 
        if (!string.IsNullOrEmpty(Convert.ToString(Id)))       
        {
            if (!IsPostBack)
            {
                BindComminssion(Id);
            }
        }
        else
        {
            Response.Redirect("CommissionMasterB2C.aspx", false);
        }
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            //string BookingFromDate = Convert.ToString(Request["From"]);
            //string BookingToDate = Convert.ToString(Request["To"]);
            //string OnwardTravelFromDate = Convert.ToString(Request["OTFromDate"]);
            //string OnwardTravelToDate = Convert.ToString(Request["OTToDate"]);
            //string ReturnTravelFromDate = Convert.ToString(Request["RTFromDate"]);
            //string ReturnTravelToDate = Convert.ToString(Request["RTToDate"]);
            string BookingClassInclude = Convert.ToString(TxtBookingClassIn.Text);
            string BookingClassExclude = Convert.ToString(TxtBookingClassEx.Text);
            string FareBasisInclude = Convert.ToString(TxtFareBasisIn.Text);
            string FareBasisExclude = Convert.ToString(TxtFareBasisEx.Text);
            string OrginAirportInclude = Convert.ToString(TxtOrginAirportIn.Text);
            string OrginAirportExclude = Convert.ToString(TxtOrginAirportEx.Text);
            string DestinationAirportInclude = Convert.ToString(TxtDestAirportIn.Text);
            string DestinationAirportExclude = Convert.ToString(TxtDestAirportEx.Text);
            string FlightNoInclude = Convert.ToString(TxtFlightNoIn.Text);
            string FlightNoExclude = Convert.ToString(TxtFlightNoEx.Text);
            string OperatingCarrierInclude = Convert.ToString(TxtOperatingCarrierIn.Text);
            string OperatingCarrierExclude = Convert.ToString(TxtOperatingCarrierEx.Text);
            string MarketingCarrierInclude = Convert.ToString(TxtMarketingCarrierIn.Text);
            string MarketingCarrierExclude = Convert.ToString(TxtMarketingCarrierEx.Text);

            string OriginCountryInclude = Convert.ToString(TxtOriginCountryIn.Text);
            string OriginCountryExclude = Convert.ToString(TxtOriginCountryEx.Text);
            string DestCountryInclude = Convert.ToString(TxtDestCountryIn.Text);
            string DestCountryExclude = Convert.ToString(TxtDestCountryEx.Text);

            //string AirlineCode = Convert.ToString(ddl_Pairline.SelectedValue);
            //string TripType = Convert.ToString(DdlTripType.SelectedValue);

          

            System.Collections.Generic.List<string> selectedItemsList = new System.Collections.Generic.List<string>();
            foreach (ListItem item in ListCabinClassIn.Items)
            {
                if (item.Selected)
                {
                    selectedItemsList.Add(item.Value);
                }
            }
            string CabinClassInclude = string.Join(",", selectedItemsList.ToArray());
            //string CabinClassInclude = Convert.ToString(ListCabinClassIn.SelectedValue);

            System.Collections.Generic.List<string> selectedItemsList2 = new System.Collections.Generic.List<string>();
            //foreach (ListItem item in ListCabinClassEx.Items)
            //{
            //    if (item.Selected)
            //    {
            //        selectedItemsList2.Add(item.Value);
            //    }
            //}
            string CabinClassExclude = ""; //string.Join(",", selectedItemsList2.ToArray());
            //string CabinClassExclude = Convert.ToString(ListCabinClassEx.SelectedValue);
          
           string FareType="";
            string BookingChannel="";
            string GroupType=""; 
            string AirlineCode="";
            string TripType = "";

            int flag = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(HdnId.Value)))
            {
                int Id = Convert.ToInt32(HdnId.Value);
                flag = UpdateCommissionMaster(Id, BookingClassInclude, BookingClassExclude, FareBasisInclude, FareBasisExclude, OrginAirportInclude, OrginAirportExclude,
                             DestinationAirportInclude, DestinationAirportExclude, FlightNoInclude, FlightNoExclude, OperatingCarrierInclude,
                             OperatingCarrierExclude, MarketingCarrierInclude, MarketingCarrierExclude, CabinClassInclude, CabinClassExclude,
                             FareType, BookingChannel, GroupType, AirlineCode, TripType, OriginCountryInclude, OriginCountryExclude, DestCountryInclude, DestCountryExclude);
                if (flag > 0)
                {
                    BindComminssion(Convert.ToString(HdnId.Value));
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully updated.');", true);
                }
                else
                {
                    BindComminssion(Convert.ToString(HdnId.Value));              
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record not updated, try again!!');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');window.location='CommissionMasterB2C.aspx'; ", true);
            }
           
           
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='CommissionMasterB2C.aspx'; ", true);
        }

    }
    private void BindComminssion(string Id)
    {
       
        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(Convert.ToString(Id)))
        {             
            dt=GetCommissionRecord(Convert.ToInt32(Id));           
            if (dt.Rows.Count > 0)
            {
                TxtBookingClassIn.Text= Convert.ToString(dt.Rows[0]["BookingClassInclude"]);
                TxtBookingClassEx.Text=Convert.ToString(dt.Rows[0]["BookingClassExclude"]);
                TxtFareBasisIn.Text=Convert.ToString(dt.Rows[0]["FareBasisInclude"]);
                TxtFareBasisEx.Text=Convert.ToString(dt.Rows[0]["FareBasisExclude"]);
                TxtOrginAirportIn.Text=Convert.ToString(dt.Rows[0]["OrginAirportInclude"]);
                TxtOrginAirportEx.Text=Convert.ToString(dt.Rows[0]["OrginAirportExclude"]);
                TxtDestAirportIn.Text = Convert.ToString(dt.Rows[0]["DestinationAirportInclude"]);
                TxtDestAirportEx.Text = Convert.ToString(dt.Rows[0]["DestinationAirportExclude"]);
                TxtFlightNoIn.Text = Convert.ToString(dt.Rows[0]["FlightNoInclude"]);
                TxtFlightNoEx.Text = Convert.ToString(dt.Rows[0]["FlightNoExclude"]);
                TxtOperatingCarrierIn.Text = Convert.ToString(dt.Rows[0]["OperatingCarrierInclude"]);
                TxtOperatingCarrierEx.Text = Convert.ToString(dt.Rows[0]["OperatingCarrierExclude"]);
                TxtMarketingCarrierIn.Text = Convert.ToString(dt.Rows[0]["MarketingCarrierInclude"]);
                TxtMarketingCarrierEx.Text = Convert.ToString(dt.Rows[0]["MarketingCarrierExclude"]);

                TxtOriginCountryIn.Text = Convert.ToString(dt.Rows[0]["OriginCountryInclude"]);
                TxtOriginCountryEx.Text = Convert.ToString(dt.Rows[0]["OriginCountryExclude"]);
                TxtDestCountryIn.Text = Convert.ToString(dt.Rows[0]["DestCountryInclude"]);
                TxtDestCountryEx.Text = Convert.ToString(dt.Rows[0]["DestCountryExclude"]);
                string CabinClassInclude = Convert.ToString(dt.Rows[0]["CabinClassInclude"]);
                string CabinClassExclude = Convert.ToString(dt.Rows[0]["CabinClassExclude"]);

                //var names = new List<string>(new string[] { "ragu", "raju" });                
                string[] names = CabinClassInclude.Split(',');
                foreach (ListItem item in ListCabinClassIn.Items)
                {
                    if (names.Contains(item.Value))
                        item.Selected = true;
                }

                string FareType = Convert.ToString(dt.Rows[0]["FareType"]);
                string BookingChannel = Convert.ToString(dt.Rows[0]["BookingChannel"]);
                string Active = Convert.ToString(dt.Rows[0]["Active"]);
                string Status = Convert.ToString(dt.Rows[0]["Status"]);
                string CreatedDate = Convert.ToString(dt.Rows[0]["CreatedDate"]);
                string UpdatedDate = Convert.ToString(dt.Rows[0]["UpdatedDate"]);
                string UpdatedBy = Convert.ToString(dt.Rows[0]["UpdatedBy"]);
               
                string CId = Convert.ToString(dt.Rows[0]["Id"]);
                string GroupType = Convert.ToString(dt.Rows[0]["GroupType"]);
                string CommisionOnBasic = Convert.ToString(dt.Rows[0]["CommisionOnBasic"]);
                string CommissionOnYq = Convert.ToString(dt.Rows[0]["CommissionOnYq"]);
                string CommisionOnBasicYq = Convert.ToString(dt.Rows[0]["CommisionOnBasicYq"]);
                string PlbOnBasic = Convert.ToString(dt.Rows[0]["PlbOnBasic"]);
                string PlbOnBasicYq = Convert.ToString(dt.Rows[0]["PlbOnBasicYq"]);
                string BookingFromDate = Convert.ToString(dt.Rows[0]["BookingFromDate"]);
                string BookingToDate = Convert.ToString(dt.Rows[0]["BookingToDate"]);
                string OnwardTravelFromDate = Convert.ToString(dt.Rows[0]["OnwardTravelFromDate"]);
                string OnwardTravelToDate = Convert.ToString(dt.Rows[0]["OnwardTravelToDate"]);
                string ReturnTravelFromDate = Convert.ToString(dt.Rows[0]["ReturnTravelFromDate"]);
                string ReturnTravelToDate = Convert.ToString(dt.Rows[0]["ReturnTravelToDate"]);
            }
            else
            {
                BtnSubmit.Visible = false;
            }
        }

    }
    public DataTable GetCommissionRecord(int ID)
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("SpFlightNewCommissionFilterB2C", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@Id", ID);
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "GETBYID");
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }

   
    private int UpdateCommissionMaster(int Id, string BookingClassInclude, string BookingClassExclude, string FareBasisInclude, string FareBasisExclude, string OrginAirportInclude, string OrginAirportExclude,
                        string DestinationAirportInclude, string DestinationAirportExclude, string FlightNoInclude, string FlightNoExclude, string OperatingCarrierInclude,
                        string OperatingCarrierExclude, string MarketingCarrierInclude, string MarketingCarrierExclude, string CabinClassInclude, string CabinClassExclude,
                        string FareType, string BookingChannel, string GroupType, string AirlineCode, string TripType, string OriginCountryInclude, string OriginCountryExclude, string DestCountryInclude, string DestCountryExclude)
    {
        int flag = 0;
        try
        {
            SqlCommand cmd = new SqlCommand("SpFlightNewCommissionFilterB2C", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OriginCountryInclude", OriginCountryInclude);
            cmd.Parameters.AddWithValue("@OriginCountryExclude", OriginCountryExclude);
            cmd.Parameters.AddWithValue("@DestCountryInclude", DestCountryInclude);
            cmd.Parameters.AddWithValue("@DestCountryExclude", DestCountryExclude);

            //cmd.Parameters.AddWithValue("@ReturnTravelFromDate", ReturnTravelFromDate);
            //cmd.Parameters.AddWithValue("@ReturnTravelToDate", ReturnTravelToDate);
            //cmd.Parameters.AddWithValue("@CommisionOnBasic", CommisionOnBasic);
            //cmd.Parameters.AddWithValue("@CommissionOnYq", CommissionOnYq);
            //cmd.Parameters.AddWithValue("@CommisionOnBasicYq", CommisionOnBasicYq);
            //cmd.Parameters.AddWithValue("@PlbOnBasic", PlbOnBasic);
            //cmd.Parameters.AddWithValue("@PlbOnBasicYq", PlbOnBasicYq);
            cmd.Parameters.AddWithValue("@Id", Id);            
            cmd.Parameters.AddWithValue("@BookingClassInclude", BookingClassInclude);
            cmd.Parameters.AddWithValue("@BookingClassExclude", BookingClassExclude);
            cmd.Parameters.AddWithValue("@FareBasisInclude", FareBasisInclude);
            cmd.Parameters.AddWithValue("@FareBasisExclude", FareBasisExclude);
            cmd.Parameters.AddWithValue("@OrginAirportInclude", OrginAirportInclude);
            cmd.Parameters.AddWithValue("@OrginAirportExclude", OrginAirportExclude);
            cmd.Parameters.AddWithValue("@DestinationAirportInclude", DestinationAirportInclude);
            cmd.Parameters.AddWithValue("@DestinationAirportExclude", DestinationAirportExclude);
            cmd.Parameters.AddWithValue("@FlightNoInclude", FlightNoInclude);
            cmd.Parameters.AddWithValue("@FlightNoExclude", FlightNoExclude);
            cmd.Parameters.AddWithValue("@OperatingCarrierInclude", OperatingCarrierInclude);
            cmd.Parameters.AddWithValue("@OperatingCarrierExclude", OperatingCarrierExclude);
            cmd.Parameters.AddWithValue("@MarketingCarrierInclude", MarketingCarrierInclude);
            cmd.Parameters.AddWithValue("@MarketingCarrierExclude", MarketingCarrierExclude);
            cmd.Parameters.AddWithValue("@CabinClassInclude", CabinClassInclude);
            cmd.Parameters.AddWithValue("@CabinClassExclude", CabinClassExclude);
            cmd.Parameters.AddWithValue("@FareType", FareType);
            cmd.Parameters.AddWithValue("@BookingChannel", BookingChannel);
            //cmd.Parameters.AddWithValue("@Active", Active);
            //cmd.Parameters.AddWithValue("@Status", Status);
            cmd.Parameters.AddWithValue("@GroupType", GroupType);
            cmd.Parameters.AddWithValue("@AirlineCode", AirlineCode);
            cmd.Parameters.AddWithValue("@TripType", TripType);
            cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UID"]));
            cmd.Parameters.AddWithValue("@ActionType", "FILTERUPDATE");           
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }
        return flag;

    }


}