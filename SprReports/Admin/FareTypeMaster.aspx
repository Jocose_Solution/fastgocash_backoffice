﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="FareTypeMaster.aspx.cs" Inherits="SprReports_Admin_FareTypeMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
        <div class="col-md-12 " >
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Result Display Master</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Fare Type :</label>
                                    <asp:DropDownList ID="FareDDl" runat="server" OnSelectedIndexChanged="FareDDl_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" >
                                        </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                     <div class="form-group">
                                     <label for="exampleInputPassword1">Display Result Name</label>
                                    <asp:TextBox ID="txtDisplayResultName" runat="server" CssClass="form-control" onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz ');" oncopy="return false" onpaste="return false" MaxLength="15"></asp:TextBox>
                                </div>
                                  
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                     <label for="exampleInputPassword1">Remark</label>
                                    <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" onkeypress="return keyRestrict(event,' 1234567890abcdefghijklmnopqrstuvwxyz');" oncopy="return false" onpaste="return false" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"></label>
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Update" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" OnClientClick="return Check();" /> 
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>                        
                        <div class="clear"></div>                        
                        <div class="row">

                            <div class="col-md-12">
                                <div id="DivMsg" runat="server" style="color: red;"></div>      
                                <br />
                                <div id="DivMsgExclued" runat="server" style="color: red;"></div>                            
                            </div>
                        </div>


                        <div class="row">
                             <div class="col-md-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="grd_P_IntlDiscount" runat="server" AutoGenerateColumns="false"
                                            CssClass="table" GridLines="None" Width="100%" PageSize="100"  AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging">
                                            <Columns> 
                                                 <asp:TemplateField HeaderText="ID">
                                                    <ItemTemplate>
                                                       <asp:Label ID="lblId" runat="server" Text='<%#Eval("Id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                
                                                <asp:TemplateField HeaderText="FareType">
                                                    <ItemTemplate>                                                        
                                                        <asp:Label ID="lblFareType" runat="server" Text='<%#Eval("FareType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Display_Result_Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDisplayName" runat="server" Text='<%#Eval("DisplayName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remark">
                                                    <ItemTemplate>                                                        
                                                        <asp:Label ID="lblRemark" runat="server" Text='<%#Eval("Remark") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="UpdatedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUpdatedDate" runat="server" Text='<%#Eval("UpdatedDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="UpdatedBy">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUpdatedBy" runat="server" Text='<%#Eval("UpdatedBy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>    
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records Found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidActionType" runat="server" Value="select" />
    </div>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script> 
    <script type="text/javascript">        
        function Check() {
            if ($("#ctl00_ContentPlaceHolder1_FareDDl").val() == "0") {
                alert("Select Fare Type.");
                $("#ctl00_ContentPlaceHolder1_FareDDl").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_txtDisplayResultName").val() == "") {
                alert("Enter Display Name.");
                $("#ctl00_ContentPlaceHolder1_txtDisplayResultName").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_txtRemark").val() == "") {
                alert("Enter Remark.");
                $("#ctl00_ContentPlaceHolder1_txtRemark").focus();
                return false;
            }
        }

        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>
</asp:Content>


